const express = require('express');
const router = express.Router();
const Dev = require("../src/models/Dev");

router.get('/', (req,res) => {
	res.send('Greetings from the node environment')
});

//write a route handler for get requests received on the /developers URI
router.get('/developers', (req, res) => {
	//find method is a promise that will return an object once it resolves
	Dev.find({})
	//take the returned object from the above promise as a parameter name devs
	.then(devs => {
		res.json(devs)
	})
});

router.get('/developers/:id', (req, res) => {
	//find method is a promise that will return an object once it resolves
	Dev.find({_id: req.params.id})
	//take the returned object from the above promise as a parameter name devs
	.then(dev => {
		res.json(dev)
	})
});

router.post('/developers', (req, res, next) => {
	Dev.create(req.body)
	.then(dev => {
		res.send(dev)
	})
	.catch(next)
});

router.put('/developers/:id', (req,res, next) => {
	Dev.findOneAndUpdate({_id: req.params.id}, req.body) 
	.then(() => {	
		Dev.findOne({_id: req.params.id})
		.then(dev => {
			res.send(dev)
		})
	})
	.catch(next)
});

router.delete('/developers/:id', (req,res, next) => {
	//pass in a query document to the findOneAndDelete method to selectively query yout method
	Dev.findOneAndDelete({_id: req.params.id}) 
	.then(dev => {
		res.send(dev)
	})
	.catch(next)
});

module.exports = router;
