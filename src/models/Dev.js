const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const DevSchema = new Schema ({
	name: String, //define a fieled called name with a data type String
	batch: Number,
	portfolio: String,
	hired: Boolean
});

//use this model for a 'devs' collection in mongoDB
//the schema defined above will be used when creating new dev documents
module.exports = mongoose.model('Dev',DevSchema);