const express = require('express');
//require body parser to extract data from json in client request
const bodyParser = require('body-parser');
//specify the location of our config file
const config = require('../src/config');
//require mongoose to manage connections with mongoDB
const mongoose = require('mongoose');
//connect to mongoDB atlas
mongoose.connect(config.databaseURL, {useNewUrlParser: true});
//display a statu message ince successfully connected
mongoose.connection.once('open', () => {
	console.log('Remote database connection established')
});
//require our Dev model
const Dev = require('../src/models/Dev');

//initialize the express app
const app = express();

const tuittDevRouter = require('../routes/api');

// this is to set the bodyParser as middleware
app.use(bodyParser.json());

 //this is a route handler where get is being passed to the URL for receiving get request on the /
app.use(tuittDevRouter);

app.use((err,req,res,next) => {
	res.status(422).send({error: err.message})
});



//designate the port where our API will listen on
app.listen(config.port, () => {
	console.log(`API server is running at http://localhost:${config.port}`)
});