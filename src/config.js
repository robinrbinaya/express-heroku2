//export the properties defined within to be accessed by other modules outside this module. Module is the file.
module.exports = {
	port: process.env.PORT || 5000,
	databaseURL: process.env.DATABASE_URL || 'mongodb+srv://robinrbinaya:robinrbinaya@clusterfreerobin-duouf.mongodb.net/onlineDB?retryWrites=true'
};